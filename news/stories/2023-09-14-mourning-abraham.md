---
title: The Debian Project mourns the loss of Abraham Raji
---

The Debian Project has lost a member of its community. On 13th September
2023 Abraham Raji was involved in a fatal accident during a kayaking
trip.

Abraham was a popular and respected Debian Developer as well a prominent
free software champion in his home state of Kerala, India. He was a
talented graphic designer and led design and branding work for DebConf23
and several other local events in recent years. Abraham gave his time
selflessly when mentoring new contributors to the Debian project, and he
was instrumental in creating and maintaining the Debian India website.

The Debian Project honors his good work and strong dedication to Debian
and Free Software. Abraham's contributions will not be forgotten, and
the high standards of his work will continue to serve as an inspiration
to others.

Mirrored from: https://www.debian.org/News/2023/20230914
