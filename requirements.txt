# Available in buster-backports
Django>=3.0,<4.0

# Available in Debian
PyYAML
django-countries
# https://github.com/SmileyChris/django-countries/issues/410
# django-countries should depende on this:
asgiref
psycopg2
python-memcached

# From the cheeseshop
bleach
bleach-allowlist
django-crispy-forms>=1.7.0,<2
mdx_linkify==2.1
mdx_staticfiles==0.1
stripe==2.60.0
wafer==0.14
wafer-debconf==0.11.10

# unused but a dependency of django-bakery
# pinned to avoid having to update it every day
boto3==1.18.44
botocore==1.21.44
