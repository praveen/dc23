all_days: &all_days
  - 2023-09-09
  - 2023-09-10
  - 2023-09-11
  - 2023-09-12
  - 2023-09-13
  - 2023-09-14
  - 2023-09-15
  - 2023-09-16
  - 2023-09-17

on_call_slots: &on_call_slots
  - start: '10:00'
    end: '12:30'
  - start: '15:00'
    end: '17:30'
  - start: '17:30'
    end: '19:00'

tasks:

  - category: Video
    name: Talk Meister
    description: >
      The Talk Meister introduces the speaker, keeps track of the time and
      coordinates the question and answer sessions, if there is one. At the end
      of the talk they also thank the speaker.

      For BoFs, the Talk Meister role is assumed by the person who leads the
      BoF.

      Speak to Nattie at the front desk first, before signing up for a Talk
      Meister task.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Room Coordinator
    description: >
      The room coordinator is responsible for ensuring that all equipment is
      working, and that all necessary volunteers are present, set up, and know
      how to do their role. If someone does not arrive in time, then the
      coordinator must find someone to replace them, or take their place.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Director
    description: >
      The Director controls what is recorded and what goes out onto the stream.
      They decide what video sources should be shown on stream. They are also
      responsible for monitoring the stream for quality assurance.

      Detailed documentation on the Director's tasks can be found in the
      Director's guide.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Camera operator
    description: >
      There are two cameras in the room, each operated by one team member.
      Generally speaking, one camera is meant to point at the speaker, and the
      other at the audience. However, these allocations can change if needed.

      For example, during the talks, the audience camera can be used as a
      second shot of the speaker. The speaker camera can also be pointed to the
      audience, if there is a discussion with many people distributed around
      the room, for instance.

      Detailed documentation on the Camera operator's tasks can be found in the
      Camera operator's guide.
    nbr_volunteers_min: 2
    nbr_volunteers_max: 2
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Sound technician
    description: >
      The sound technician operates the audio mixer and is responsible for
      ensuring that audio is clear and intelligible on the stream and
      recordings as well as in the room itself. The sound technician must know
      how to operate the audio devices and mixer, as this role is critical for
      not only the video streaming and recording, but also for the talk itself.

      Detailed documentation on the Sound technician's tasks can be found in
      the Sound technician's guide.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 1
    required_permission: volunteers.accept_video_tasks
    video_task: true

  - category: Video
    name: Videoteam member on call
    description: >
      For all sessions, there is a core videoteam member who can be reached
      quickly to help solve problems with the infrastructure.

      If this person is not in the talk room, they can reached on
      #debconf-video.
    nbr_volunteers_min: 1
    days:
      - 2023-09-10
      - 2023-09-11
      - 2023-09-12
      - 2023-09-13
      - 2023-09-14
      - 2023-09-15
      - 2023-09-16
    hours: *on_call_slots
    required_permission: volunteers.accept_video_tasks

  - category: Front desk
    venues:
      - Front desk
    name: Front desk
    description: |
      You will help the Front desk team in the registration process and
      preparing swag.
      If you haven't done this task at DebConf before, talk to Front desk staff
      before signing up.
    nbr_volunteers_min: 1
    nbr_volunteers_max: 3
    days: *all_days
    hours: *on_call_slots
