from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site
from django.template import Context, Template

from register.models import Attendee


SUBJECT = '{{ WAFER_CONFERENCE_NAME }}: Reimbursements for Conference Dinner available'
TXT = '''Dear {{ name }},

Our apologies, because we cancelled the conference dinner, we're offering
reimbursement to everyone who has paid for it.

This still cost us money. Due to the last-minute cancellation, we are only
expecting to able to reclaim a small part of our expenses for the dinner.

If you'd like to be reimbursed for the meal you didn't have, we'd be happy to
oblige. Please reply to this email if you'd like a reimburesment.
Reimbursements will be made directly to the credit card that was charged and
the invoice will be amended appropriately.

Invoice number #{{ invoice_number }}

We hope you enjoyed {{ WAFER_CONFERENCE_NAME }},

The {{ WAFER_CONFERENCE_NAME }} Registration Team
'''


class Command(BaseCommand):
    help = "Offer reimbursement to attendees who paid for the conference dinner"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something')
        parser.add_argument('--site', type=int, default=1,
                            help='Django site ID, default: 1')

    def badger(self, attendee, invoice, dry_run, site):
        user = attendee.user
        context = Context({
            'invoice_number': invoice.reference_number,
            'name': user.userprofile.display_name(),
            'WAFER_CONFERENCE_NAME': site.name,
        })

        txt = Template(TXT).render(context)
        subject = Template(SUBJECT).render(context)
        to = user.email
        if dry_run:
            print('I would badger:', to)
            return
        email_message = EmailMultiAlternatives(subject, txt, to=[to])
        email_message.send()

    def find_invoice(self, user):
        for invoice in user.invoices.filter(status='paid'):
            for line in invoice.lines.all():
                ref = line.reference
                if ref.startswith(settings.INVOICE_PREFIX):
                    ref = ref[len(settings.INVOICE_PREFIX):].split('-')[0]
                if ref == 'CONFDINNER':
                    return invoice
        return None

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')
        site = Site.objects.get(id=1)
        for attendee in Attendee.objects.all():
            user = attendee.user
            if not user.userprofile.is_registered():
                continue
            invoice = self.find_invoice(user)
            if invoice:
                self.badger(attendee, invoice, dry_run, site)
