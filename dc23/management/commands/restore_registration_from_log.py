# -*- coding: utf-8 -*-
import json
import re
import sys

from django.core.mail import EmailMultiAlternatives
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.template import Template, Context

from register.dates import parse_date
from register.models import Attendee, Accomm, AccommNight, Food, Meal


SUBJECT = "Re: DebConf 23: Registration Automatically Cancelled"

TEMPLATE = """\
Dear {{ name }},

Our apologies, we mistakenly cancelled your registration this evening.
A management script overstepped its bounds and we didn't have sufficient review
to catch the problem before doing damage.

We've done our best to restore it things to the way they were.
Please go through the registration form again and double-check that we got it
right.
<https://debconf23.debconf.org/register/>

While you're there, please set final arrival and departure dates, select the
"My dates are final" entry and tick the "I confirm my attendance" box. Then go
through the rest of the form verifying your data and Save.

If you no longer plan to attend DebConf 23, go to:
<https://debconf23.debconf.org/register/unregister>
and click the "Unregister" button.

Again, our sincere apologies, we didn't mean to do this.

The DebConf Registration Team
"""

class Command(BaseCommand):
    help = 'Restore registration from the log and apologies'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something')

    def badger(self, user, data, dry_run):
        attendee_data = data.pop('attendee')
        attendee_data.pop('user')
        addresses = attendee_data.pop('addresses')
        assert not addresses
        attendee, created = Attendee.objects.get_or_create(user=user, defaults=attendee_data)

        accomm_data = data.pop('accomm')
        accomm_data.pop('attendee')
        accomm_data.setdefault('option', 'shared')
        nights = accomm_data.pop('nights')
        accomm, created = Accomm.objects.get_or_create(attendee=attendee, defaults=accomm_data)
        if created:
            for night in nights:
                date = parse_date(night.split('_')[1])
                accomm.nights.add(AccommNight.objects.get(date=date))

        food_data = data.pop('food')
        food_data.pop('attendee')
        meals = food_data.pop('meals')
        food, created = Food.objects.get_or_create(attendee=attendee, defaults=food_data)

        if created:
            for meal in meals:
                meal, date = meal.split('_')
                date = parse_date(date)
                food.meals.add(Meal.objects.get(meal=meal, date=date))

        child_care = data.pop('child_care')
        assert child_care == {}

        data.pop('bursary')
        data.pop('profile')
        data.pop('user')
        assert not data.keys()

        name = user.userprofile.display_name()
        to = user.email

        ctx = Context({
            'name': name,
        })

        template = Template(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        User = get_user_model()
        while True:
            line = sys.stdin.readline().strip()
            if not line:
                break

            m = re.match(r'[0-9:, -]+INFO\s+register\s+User registered: '
                         r'user=(\S+) updated=(?:True|False) data=(.*)', line)
            user = User.objects.get(username=m.group(1))
            data = json.loads(m.group(2))
            self.badger(user, data, dry_run)
