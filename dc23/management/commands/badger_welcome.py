# -*- coding: utf-8 -*-
from collections import namedtuple

from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Accomm, Attendee, Food


SUBJECT = "Welcome to DebConf23! (arrival details)"

TEMPLATE = """\
Dear {{ name }},

Welcome to DebConf23, hosted at the Infopark Kochi, Kerala in India.

Please read the contents of this mail and the Conference Venue page [1] in the
web site carefully.  They contain important information about your travel to
Kochi, and DebConf23.

[1] https://debconf23.debconf.org/about/venue/

== Before departing for Kochi ==

Print this e-mail and the relevant information (including instructions how to
reach the venue) from the website.

Check your data, and report any errors and changes (including delays or
cancellation) as soon as possible to <registration@debconf.org>:

Arriving: {{ attendee.arrival|date:"DATETIME_FORMAT" }}
Departing: {{ attendee.departure|date:"DATETIME_FORMAT" }}{% if food %}
Meals requested:{% for day in meals %}
{{ day.date|date:"SHORT_DATE_FORMAT" }}: {{ day.meals|join:", "|title }}{% endfor %}
Diet: {{ food.diet|default:"Whatever is provided" }}{% if food.special_diet %}
Details: {{ food.special_diet }}{% endif %}{% else %}
No conference-organised food requested.{% endif %}

Familiarize yourself with the Codes of Conduct for both Debian [2] and
DebConf [3].  If you are a victim of harassment, or observe it, you can
contact<community@debian.org> or members of the on-site anti-harassment team
[4].  The community team will be represented on-site by Sruthi Chandran.

For any other kind of problem, the Front Desk and Organisation team may be of
help.

[2] https://www.debian.org/code_of_conduct
[3] http://debconf.org/codeofconduct.shtml
[4] https://debconf23.debconf.org/about/coc/

Check your travel documents.  Will your passport remain valid for the duration
of your stay?  Do you have all necessary travel authorizations (such as a
visa)?

Notify your bank that you'll be travelling to India to avoid having your
card(s) blocked.  PLEASE NOTE: not all shops, even those that take cards, are
equipped to deal with international cards, so please have some cash on hand
when you arrive. Cash can be obtained at a bureau de change before you leave
your home country, or at an ATM at the airport on arrival.

Do you have travel insurance? Be sure to also check with your insurance if
you're covered for lost or stolen items, such as electronics.

Take a COVID-19 self-test, if you can get your hands on one. You wouldn't want
to infect other people at the conference or on the plane.

Cheese & Wine allowance:
You may bring reasonable amounts of cheese.
Up to 2 ℓ of any alcohol duty free, and more if you are willing to pay the
customs duty for additional quantities.
More information:
https://old.cbic.gov.in/resources/htdocs-cbec/guide_for_travellers/GUIDE_FOR_TRAVELLERS_v5.pdf

== Your Accommodation Details ==
{% if not accomm %}
You don't have any conference-provided accommodation, or you haven't yet paid
your invoices.  Remember to print the details of your self-organised
accommodation in case you're asked for proof by immigration officials.{% else %}
=== On-site Accommodation ===

According to our records, you're registered to stay on-site on the following
days:
{% for stay in stays %}
Check-in: {{ stay.0|date }}
Checkout: {{ stay.1|date }}{% endfor %}
Room: {{ accomm.room|default:"not assigned yet" }}{% if roommates %}
Roommates: {{roommates|join:", "|default:"none"}}
(These are subject to change){% endif %}

Room numbers beginning with TEMP will be reassigned by the hotel on check-in.

Check-in for the conference will be done at Front Desk, on second floor of the
hotel, Four Points by Sheraton, Kochi. You just need to go there and give your
name.

Please check into the hotel at their reception first, then come to Front Desk
on the 2nd floor. If you arrive late and the Front Desk is closed, check in
with them in the morning.
The front desk phone number is +91 84969 41434.

Most on-site attendees are sharing a room with others, so if you're sensitive
to noise when sleeping, consider bringing earplugs.

https://debconf23.debconf.org/about/accommodation/{% endif %}

== What to Bring ==

An international adapter (if required) and power supplies for all your devices.
Information about plugs used in India, and the relevant adapters, is available
at https://www.power-plugs-sockets.com/india/.

Summer clothes.  It's hot in Kochi, but it's also the rainy season.  The
temperature is expected to range from 25 to 35°C.  It may rain all day.
Luckily, an umbrella will be provided as part of your swag.

Don't forget sunscreen if you plan to enjoy the outdoors. Mosquito repellent is
also a very good idea.

Printed PGP/GPG fingerprint slips, if you want to have your key signed by your
peers, outside the main keysigning party.

A roll of toilet paper, if you aren't comfortable with using an asian bidet
shower. Don't expect public toilets to have toilet paper. Front desk will have
a few rolls available if you are unable to obtain one on your own.

== Arrival in Kochi ==

https://debconf23.debconf.org/about/venue/ has details of getting to Kochi and
the venue. Multiple modes of transport are described on this page, both public
and private. See https://wiki.debian.org/DebConf/23/TravelCoordination to find
other attendees arriving around the same time as you, and to coordinate with
them.

You can buy local SIM cards at the airport. There will also be a representative
from Airtel on-site on the 3rd and 10th of September. Airtel provides the
opportunity to get a SIM and to recharge those at reduced rates. These SIMs can
also be recharged online, which is not possible for SIMs obtained through other
local vendors. Those who are staying more than 28 days shouldn't go to local
vendors for the same plan, but can do it online at https://www.airtel.in/

If you are planning to take a taxi, please book it at the pre-booked taxi desk
in the arrivals hall.  Uber is also available if need be.

=== Taxis ===

Taxi from Kochi International Airport to Infopark, Kakkanad costs around
1000-1200 INR (12-15 USD).

Be sure to book your taxi at the pre-booked taxi desk in the arrivals hall.  It
will be too late once you have exited the arrivals hall!

== Front Desk ==

In order to gain access to the conference venue, you must first check in at
Front Desk. There, you will be given your attendee badge and - if you
registered early enough - your conference swag.

The Front Desk is located on the second floor of the hotel.  It is open from
10:00 to 18:00 daily.

If you run into any problems, you can call the DebConf 23 Front Desk at
+91 84969 41434, or message the same number on Signal.

Save this in your phone now 🙂

Have a safe trip.  We look forward to seeing you in Kochi!

The DebConf Team
"""

Stay = namedtuple('Stay', ('checkin', 'checkout'))
Meal = namedtuple('Meal', ('date', 'meals'))


def meal_sort(meal):
    return ['breakfast', 'lunch', 'dinner'].index(meal)


class Command(BaseCommand):
    help = 'Send welcome emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email

        try:
            accomm = attendee.accomm
            roommates = accomm.get_roommates()
            if roommates:
                roommates = [
                    '%s (%s)' % (attendee.user.userprofile.display_name(), attendee.user.username)
                    for attendee
                    in roommates
                ]
        except Accomm.DoesNotExist:
            accomm = None
            roommates = None

        if attendee.billable() and not attendee.paid():
            accomm = None
            roommates = None

        try:
            food = attendee.food
        except Food.DoesNotExist:
            food = None

        if accomm:
            stays = list(attendee.accomm.get_stay_details())
        else:
            stays = None

        meals = []
        if food:
            by_day = {}
            for meal in food.meals.all():
                by_day.setdefault(meal.date, []).append(meal.meal)
            for date, days_meals in sorted(by_day.items()):
                meals.append(Meal(date, sorted(days_meals, key=meal_sort)))

        ctx = {
            'accomm': accomm,
            'roommates': roommates,
            'attendee': attendee,
            'food': food,
            'meals': meals,
            'name': name,
            'stays': stays,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.all()
        if options['username']:
            queryset = queryset.filter(user__username=options['username'])

        for attendee in queryset:
            self.badger(attendee, dry_run)
