---
name: Job Fair
---

Our annual job fair during DebConf connects our qualifying sponsors with
DebConf participants interested in new professional opportunities.

The Job Fair will be held outside the Ponmudi hall at second floor, Four Points hotel, on Sunday 10 September 2023 from 01:30 PM - 03:30 PM.

We invite job seekers and employers to this. A space will be provided where you can sit at a place and get bunch of people recruited from the best or can stroll around and find an interesting job for you.

If you plan to hire someone, it would be ideal if you could bring a (branded) table cloth, roll-up banners and company info material/swag to decorate your space.
