---
name: DebCamp
---

# What is DebCamp

We received some questions about "what is DebCamp" and "how is DebCamp different
from [DebConf](https://www.debconf.org)". We wrote this text to help you decide
whether to come early to Kochi to take part in DebCamp from September 3rd to 9th
(the week before DebConf23).

DebCamp is the hacking session right before DebConf. DebCamp usually occurs in
the week before DebConf itself. It's a week for Debian contributors to focus on
their Debian-related projects, tasks, or problems uninterrupted.

DebCamps are largely self-organized since it is for people who would like to do
some work. Some prefer to work alone, while some prefer to participate in or
organize [sprints](https://wiki.debian.org/Sprints). Both are great, although we
strongly encourage you to plan your DebCamp week in advance.

If you are around and looking for something to do, please get in touch with the
DebConf organization team on the #debconf-team IRC channel. You can also find
more information under the
[volunteers' section](https://debconf23.debconf.org/volunteers) on the main
DebConf23 page.

We will not have a regular schedule with talks, workshops, and BoFs during
DebCamp (from 3rd to 9th this year). The regular schedule will happen during
DebConf23 (from 10th to 17th). You can take a look at
[this page](https://wiki.debian.org/DebConf/22/DebCamp) to know what happened
during DebCamp in 2022.
