---
name: COVID-19
---

# COVID-19 Information (as of August 2023)

## Entry Requirements

### Before Travel
All attendees should preferably be fully vaccinated as per the approved primary
schedule of vaccination against COVID-19 in their Country.

### During Travel
Precautionary measures to be followed (preferable use of masks and following
physical distancing) in flights/travel and at all points of entry.

## Conference Policy
The DebConf23 organizers recommend regular testing policy and usage of face
masks. We recommend attendees to wear masks even if you don't show any symptoms,
for your own safety, especially when packed in confined spaces with many people.

Every attendee should be fully vaccinated. If you are not vaccinated, you will
need to take a COVID-19 test upon arrival.

### COVID-19 Testing
COVID-19 Antigen self test kits and face masks will be available at Front desk
upon requests.

All attendees should self-monitor their health post arrival. Any attendee having
symptoms of COVID-19 shall self-isolate as per standard protocol i.e. the said
attendee should be wearing mask, isolated and segregated from other attendees
and seek medical care as per health protocols.

### Isolation on site
Facilities for self isolation can be arranged at hotel.

### Changes
This guideline is drafted in **August 2023** and **might be modified** later,
depending on the changes in global situation of pandemic(s) and local
guidelines.
